import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import {
  HOME,
  ABOUT_ME,
  SKILLS,
  MY_PROJECTS,
  MY_PROJECTS_DETAIL,
  CONTACT,
} from "./config/router/paths";
import AboutMe from "./components/views/AboutMe/AboutMe";
import Contact from "./components/views/Contact/Contact";
import Home from "./components/views/Home/Home";
import MyProjects from "./components/views/MyProjects/MyProjects";
import MyProjectsDetail from "./components/views/MyProjectsDetail/MyProjectsDetail";
import Skills from "./components/views/Skills/Skills";

function App() {
  return (
    <div>
      <Router>
        <Switch>
          <Route path={SKILLS} component={Skills} />
          <Route path={MY_PROJECTS_DETAIL} component={MyProjectsDetail} />
          <Route path={MY_PROJECTS} component={MyProjects} />
          <Route path={CONTACT} component={Contact} />
          <Route path={ABOUT_ME} component={AboutMe} />
          <Route path={HOME} component={Home} exact />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
