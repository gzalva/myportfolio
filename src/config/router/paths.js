export const HOME = "/";
export const ABOUT_ME = "/about-me";
export const SKILLS = "/skills";
export const MY_PROJECTS = "/my-projects";
export const MY_PROJECTS_DETAIL = "/my-projects/:id";
export const CONTACT = "/contact";
